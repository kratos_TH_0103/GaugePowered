define([
    "dojo/_base/array",
    "dojo/query",
    "bootstrap/Tooltip",
    "gamestradamus/region"
], function(array, query, Tooltip, region) {

    var init = function() {
        var container = query('article.game')[0];

        /* Add community value tooltips */
        array.forEach(query('div.community-statistics h2 mark'), function(item) {
            query(item).tooltip({
                container: container,
                trigger: 'hover',
                placement: 'top',
                html: true,
                title: function() {
                    var title = '<p>The value an average member of the Gauge community could get from <strong>' 
                        + item.getAttribute('data-name') + '</strong> at <strong>';
                    if (region['symbol-position'] == 'before') {
                        title += region['symbol'] + region['format-dollars'](item.getAttribute('data-price'));
                    } else {
                        title += region['format-dollars'](item.getAttribute('data-price')) + region['symbol'];
                    }
                    title += '</strong></p>';
                    return title;
                }
            });
        });

        /* Add a tooltip for the histogram */
        array.forEach(query('div.similar-game-list h2 mark'), function(item) {
            query(item).tooltip({
                container: container,
                trigger: 'hover',
                placement: 'top',
                html: true,
                title: '<p>The distribution of time spent playing <strong>' + item.getAttribute('data-name') + '</strong> across all members of the Gauge community. ' +
                    'The median, <strong>' + region['format-hours'](item.getAttribute('data-hours')) + ' hours</strong> is denoted by the dotted line.</p>'
            });
        });

        /* Add the watchlist tooltip */
        query('#add-to-watchlist').tooltip({
            container: container,
            trigger: 'hover',
            placement: 'top',
            html: true,
            title: '<p>Sign in to add to your Watchlist.</p><p><img src="/images/steam-openid-login-small.png"></p>' 
        });

    };
    return init;
});
