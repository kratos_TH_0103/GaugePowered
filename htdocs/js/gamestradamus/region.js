define([
    "dojo/query",
    "gamestradamus/regions"
], function(query, REGIONS) {

    var region = 'region-us';
    var classes = query('body')[0].className.split(' ');
    for (var i = 0, len = classes.length; i < len; ++i) {
        if (classes[i].indexOf('region-') == 0) {
            region = classes[i];
            break;
        }
    }
    region = region.split('-')[1];

    return REGIONS[region];
});
