define([
    "atemi/io/jsonrpc",
    "bootstrap/Tooltip",
    "dojo/_base/event",
    "dojo/on",
    "dojo/query"
], function(
    jsonrpc,
    Tooltip,
    event,
    on,
    query
) {
    var rpc = jsonrpc('/jsonrpc');

    //--------------------------------------------------------------------------
    var bind_email_behaviour = function() {

        var message = 'Must be a valid email address';
        query('input.email', 'id-add-email').tooltip({
            trigger: 'manual',
            placement: 'left',
            html: false,
            title: function() { return message; }
        }); 

        on(query('#id-add-email'), 'submit', function(evt) {
            event.stop(evt);

            var email = query('input.email', 'id-add-email')[0]; 
            rpc.request({
                method: 'useraccountemail.create',
                params: ['select(id)', {
                    email: email.value
                }]
            }, function(data) {
                window.location.reload();

            }, function(error) {
                message = error.error.message;
                query('input.email', 'id-add-email').tooltip('show');
            });
        });
    };

    //--------------------------------------------------------------------------
    var bind_timer_behaviour = function() {
        var start = new Date();

        // Find the next Monday.
        var end = new Date(start);
            end.setUTCDate(end.getUTCDate() + ((8 - end.getUTCDay()) % 7));
            end.setUTCHours(16, 59, 59, 999);

        // When the start date is a Monday, make sure we pick the next Monday
        // after 11 am.
        if (start >= end) {
            end.setUTCDate(end.getUTCDate() + 7);
        }

        var update_timer = function() {
            var now = new Date();
            var diff = Math.floor((end - now) / 1000);
            if (diff > 0) {
                var days = Math.floor(diff / 86400);
                diff = diff % 86400;
                var hours = Math.floor(diff / 3600);
                diff = diff % 3600;
                var minutes = Math.floor(diff / 60);
                diff = diff % 60;
                var seconds = Math.floor(diff); 
            } else {
                var days = 0,
                    hours = 0,
                    minutes = 0,
                    seconds = 0;
            }

            query('.days >.count', 'timer')[0].innerHTML = days;
            query('.hours >.count', 'timer')[0].innerHTML = ('0' + hours).slice(-2);
            query('.minutes >.count', 'timer')[0].innerHTML = ('0' + minutes).slice(-2);
            query('.seconds >.count', 'timer')[0].innerHTML = ('0' + seconds).slice(-2);
        };
        update_timer();
        window.setInterval(update_timer, 1000);
    }; 

    //--------------------------------------------------------------------------
    var init = function() {
        bind_timer_behaviour();
        bind_email_behaviour();
    };
    return init;
});
