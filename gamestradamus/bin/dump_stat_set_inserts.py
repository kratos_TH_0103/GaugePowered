# -*- coding: utf-8 -*-
import sys
import string
import datetime
from geniusql import logic
from gamestradamus import cli, units, timeutils
from gamestradamus.taskqueue import tasks
from gamestradamus.bin.find_equivalent_games import find_duplicates, clean_name

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    """Run this once after deploying the StatSet change to ensure they are mapped.

    This will go through and look at the canonical_game_id set for all games and
    update them to be a part of the same stat set.
    """

    store = cli.storage_manager(conflicts='repair')
    sandbox = store.new_sandbox()


    games = sandbox.recall(units.Game, lambda g: g.stat_set_id is not None)
    for game in games:
        print "UPDATE \"Game\" SET stat_set_id=%s WHERE id=%s" % (game.stat_set_id, game.id)

    store.shutdown()
