
import random
import datetime
from gamestradamus import cli, units, timeutils
from gamestradamus.taskqueue import tasks

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    """Update some UserAccounts from Steam that haven't been updated in a while."""

    store = cli.storage_manager()
    sandbox = store.new_sandbox()

    cut_off = timeutils.utcnow() - datetime.timedelta(days=7)
    rows, cols = sandbox.store.db.fetch("""
        SELECT "UserAccount".openid
        FROM "UserAccountAttributes"
        INNER JOIN "UserAccount"
            ON "UserAccount".id = "UserAccountAttributes".user_account_id
        WHERE "UserAccountAttributes".games_last_updated < '%s'
            OR "UserAccountAttributes".games_last_updated IS NULL
            AND "UserAccount".openid <> 'Metascore'
        ORDER BY
            "UserAccountAttributes".games_last_updated IS NOT NULL ASC,
            "UserAccountAttributes".games_last_updated ASC
        LIMIT 5000
        """ % (cut_off, )
    )
    for row in random.sample(rows, 10):
        steam_id_64 = row[0].rsplit('/', 1)[-1]
        result = tasks.steam_update_hours_in_game.apply_async(**{
            'args': [steam_id_64],
            'routing_key': 'tasks.plebs.update-games'
        })         
    store.shutdown()

