"""Gamestradamus specific emails handling.
"""
from __future__ import absolute_import

from email.message import Message
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from jinja2.ext import loopcontrols
from jinja2 import Environment, PackageLoader
import base64
import jinja2
import pickle

import cream
from cream.plugins import JinjaPlugin, FragmentCacheExtension, MemoryCache
from gamestradamus import cli, filters
from plumbing.email import units


#-------------------------------------------------------------------------------
def add_email_to_queue(
    to_address, subject, text,
    from_address=None, html=None, attachments=None, sandbox=None,
    **kwargs
):
    """Adds an email to our internal email queue.

    The minimum required parameters are:
        - to_address
        - subject
        - text

    Optional parameters are:
        - from_address - defaults to gauge.email.server_address in config
        - html
        - attachments
        - sandbox
    """
    # Get access to the database if we don't already have access
    should_flush_sandbox = False
    if not sandbox:
        # If we created the sandbox, we should flush it at the end.
        should_flush_sandbox = True
        store = cli.storage_manager()
        sandbox = store.new_sandbox()

    if not from_address:
        config = cli.get_configuration()
        from_address = config.get('global', {}).get('gauge.email.server_address')

    if attachments == None:
        attachments = []

    message = MIMEMultipart('alternative')
    message['Subject'] = subject
    message['From'] = from_address
    message['To'] = to_address

    if text:
        message.attach(MIMEText(text, 'plain', 'utf-8'))
    if html:
        message.attach(MIMEText(html, 'html', 'utf-8'))

    for attachment in attachments:
        content_type = attachment.get('content_type').split('/')
        if len(content_type) != 2:
            raise RuntimeError('Invalid content_type: "%s"' % attachment.get('content_type'))

        part = MIMEBase(*content_type)
        part.set_payload(attachment.get('content'))
        part.add_header('Content-Transfer-Encoding', 'base64')
        part.add_header('Content-Disposition', 'attachment; filename=%s' % attachment.get('name'))
        message.attach(part)

    email = units.Email(**{
        'to': to_address,
        'from_': from_address,
        'subject': subject,
        'message': base64.b64encode(pickle.dumps(message, pickle.HIGHEST_PROTOCOL)),
        'options': kwargs.get('options', '{}'),
    })
    sandbox.memorize(email)

    # flush the sandbox if we created it.
    if should_flush_sandbox:
        sandbox.flush_all()

#-------------------------------------------------------------------------------
def render_add_to_email_queue(
    to_address, email_name, context,
    from_address=None, attachments=None, sandbox=None,
    **kwargs
):
    """Creates emails from three templates: subject.txt, body.txt & body.html.

    Creates an email to send (and optionally adds it to the queue) using the
    typical gauge pattern of getting the subject and body from:
        - /gamestradamus/emails/<email_name>/subject.txt
        - /gamestradamus/emails/<email_name>/body.txt
        - /gamestradamus/emails/<email_name>/body.html
    """

    # Getting access to this should be not duplicated here and pipe.
    env = Environment(
        loader=PackageLoader('gamestradamus', 'templates',),
        extensions=[FragmentCacheExtension, loopcontrols]
    )
    env.filters.update(filters.filters)

    subject_path = "/gamestradamus/emails/%s/subject.txt" % email_name
    templates = [
            ('text', '/gamestradamus/emails/%s/body.txt' % email_name),
            ('html', '/gamestradamus/emails/%s/body.html' % email_name),
        ]

    message = {}
    for name, template_path in templates:
        try:
            message[name] = env.get_template(template_path).render(context)
        except jinja2.TemplateNotFound:
            pass
    subject = env.get_template(subject_path).render(context)

    # Normalize the subject to spaces on one line
    subject = " ".join([x.strip() for x in subject.strip().split() if x.strip()])

    add_email_to_queue(
        to_address=to_address,
        subject=subject.strip().replace("\n", ""),
        from_address=from_address,
        text=message['text'],
        html=message.get('html'),
        attachments=attachments,
        sandbox=sandbox,
        **kwargs
    )


