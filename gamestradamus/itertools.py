"""Some additional itertools for the gamestradamus codebase
"""
import bisect

def closest_two(search_space, search_key, distance_func):
    "Finds the first two closest matches for the key within the search space."
    closest = []
    distances = []
    for value in search_space:

        # if we haven't even found two yet, go ahead and add them
        distance = distance_func(value, search_key)
        if len(closest) < 2:
            closest.append(value)
            distances.append(distance)
        else:
            max_distance = max(*distances)
            if distance <= max(*distances):
                closest.append(value)
                distances.append(distance)
                closest.pop(0)
                distances.pop(0)
            else:
                # Because the list is ordered, none of the rest of the list
                # will be closer, because this one isn't.
                return closest
    return closest

def closest_two_bisect(search_space, search_key, lo=0):
    "Finds the first two closest matches for the key within the search space."
    i = bisect.bisect_left(search_space, search_key, lo=lo)

    # if we're not at the beginning of the list, one further back might be
    # the closer to i.
    if i > 0 and i < len(search_space):
        before = abs(search_space[i-1] - search_key)
        after = abs(search_space[i] - search_key)
        if before <= after:
            i -= 1

    # guards against inappropriate indexing
    if i == 0:
        return i, i+1
    if i == len(search_space)-1:
        return i-1, i
    if i == len(search_space):
        return i-2, i-1

    # Because bisect might return the index one AFTER our closest one, we have
    # to search 3 spots
    before = abs(search_space[i-1] - search_key)
    after = abs(search_space[i+1] - search_key)
    if before <= after:
        return i-1, i
    else:
        return i, i+1

if __name__ == "__main__":
    print "Test"
    import operator
    tests = (
            (range(9), 5),
            ([0.1, 0.2, 0.3, 0.9, 1.0, 1.5, 2.0, 2.5,], 1.1),
            ([1.5, 1.0, 0.9, 0.3, 0.2,], 1.1),
            ([20.5, 80.9, 102.9, 150.0, 169.0 ], 97.0),
            ([20.5, 80.9, 102.9, 150.0, 169.0 ], 103.0),
        )
    for search_space, search_key in tests:
        print "-"*80
        print "%s -> %s = %s" % (
                search_key,
                search_space,
                closest_two(
                    search_space,
                    search_key,
                    lambda x,y: abs(x-y)
                )
            )
        print "%s -> %s = %s" % (
                search_key,
                search_space,
                closest_two_bisect(
                    search_space,
                    search_key
                )
            )
