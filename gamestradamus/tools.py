"""Gamestradamus tools.."""

import cherrypy
from cherrypy._cptools import Tool
from gamestradamus.regions import REGIONS

#-------------------------------------------------------------------------------
class WelcomeTool(Tool):
    """Detects if a user has been welcomed and agreed to the terms of service.

       If a user has not had both flags set, the tool will redirect to the
       users welcome page."""

    #---------------------------------------------------------------------------
    def __init__(self):
        Tool.__init__(self, 'before_handler', self._is_welcomed, priority=70)

    #---------------------------------------------------------------------------
    def _is_welcomed(self):
        request = cherrypy.serving.request

        if hasattr(request, 'user_account') and request.user_account:
            user_attrs = request.user_account.UserAccountAttributes()
            if not user_attrs or not user_attrs.welcomed_since:
                raise cherrypy.HTTPRedirect('/profile/welcome/')
        else:
            # Anyone not logged in, probably shouldn't be on a handler that checks for welcoming.
            raise cherrypy.HTTPRedirect('/')

cherrypy.tools.welcome = WelcomeTool()

#-------------------------------------------------------------------------------
def http_methods_allowed(methods=['GET', 'HEAD']):
    method = cherrypy.serving.request.method.upper()
    if method not in methods:
        cherrypy.response.headers['Allow'] = ", ".join(methods)
        raise cherrypy.HTTPError(405)

cherrypy.tools.allow = cherrypy.Tool('on_start_resource', http_methods_allowed)


#-------------------------------------------------------------------------------
def user_region_tool():
    """Provides the region information for the user given their preferences.
    """
    request = cherrypy.serving.request

    # Default to the US.
    request.user_region = REGIONS["us"]

    user_account = getattr(request, 'user_account')
    if not user_account:
        # TODO: geolocate people at some point?
        return

    attrs = user_account.UserAccountAttributes()
    if not attrs:
        return

    request.user_attrs = attrs
    # Set their region from their profile if logged in.
    request.user_region = REGIONS[attrs.region]
cherrypy.tools.user_region = cherrypy._cptools.Tool(
    'before_handler', user_region_tool, priority=70
)
