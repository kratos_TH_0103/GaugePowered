# coding: utf-8
"""Test the gauge region format utilities.
"""
from decimal import Decimal
import unittest
import os

from gamestradamus import regions

base_dir = os.path.dirname(os.path.abspath(__file__))


#-------------------------------------------------------------------------------
class TestPricingUtilities(unittest.TestCase):

    #--------------------------------------------------------------------------
    def test_US(self):
        self.assertEqual(u"$1.56", regions.USD.format_price(Decimal("1.56"), True))
        self.assertEqual(u"$0.56", regions.USD.format_price(Decimal("0.56"), True))
        self.assertEqual(u"$0.99", regions.USD.format_price(Decimal("0.99"), True))
        self.assertEqual(u"$0.01", regions.USD.format_price(Decimal("0.01"), True))

        self.assertEqual(u"$1.56", regions.USD.format_value(Decimal("1.56"), True))
        self.assertEqual(u"56¢", regions.USD.format_value(Decimal("0.56"), True))
        self.assertEqual(u"99¢", regions.USD.format_value(Decimal("0.99"), True))
        self.assertEqual(u"1¢", regions.USD.format_value(Decimal("0.01"), True))

        self.assertEqual(u"1.56", regions.USD.format_price(Decimal("1.56")))
        self.assertEqual(u"0.56", regions.USD.format_price(Decimal("0.56")))
        self.assertEqual(u"0.99", regions.USD.format_price(Decimal("0.99")))
        self.assertEqual(u"0.01", regions.USD.format_price(Decimal("0.01")))

        self.assertEqual(u"1.56", regions.USD.format_value(Decimal("1.56")))
        self.assertEqual(u"56", regions.USD.format_value(Decimal("0.56")))
        self.assertEqual(u"99", regions.USD.format_value(Decimal("0.99")))
        self.assertEqual(u"1", regions.USD.format_value(Decimal("0.01")))

    #--------------------------------------------------------------------------
    def test_GBP(self):
        self.assertEqual(u"£1.56", regions.GBP.format_price(Decimal("1.56"), True))
        self.assertEqual(u"£0.56", regions.GBP.format_price(Decimal("0.56"), True))
        self.assertEqual(u"£0.99", regions.GBP.format_price(Decimal("0.99"), True))
        self.assertEqual(u"£0.01", regions.GBP.format_price(Decimal("0.01"), True))

        self.assertEqual(u"£1.56", regions.GBP.format_value(Decimal("1.56"), True))
        self.assertEqual(u"56¢", regions.GBP.format_value(Decimal("0.56"), True))
        self.assertEqual(u"99¢", regions.GBP.format_value(Decimal("0.99"), True))
        self.assertEqual(u"1¢", regions.GBP.format_value(Decimal("0.01"), True))

        self.assertEqual(u"1.56", regions.GBP.format_price(Decimal("1.56")))
        self.assertEqual(u"0.56", regions.GBP.format_price(Decimal("0.56")))
        self.assertEqual(u"0.99", regions.GBP.format_price(Decimal("0.99")))
        self.assertEqual(u"0.01", regions.GBP.format_price(Decimal("0.01")))

        self.assertEqual(u"1.56", regions.GBP.format_value(Decimal("1.56")))
        self.assertEqual(u"56", regions.GBP.format_value(Decimal("0.56")))
        self.assertEqual(u"99", regions.GBP.format_value(Decimal("0.99")))
        self.assertEqual(u"1", regions.GBP.format_value(Decimal("0.01")))

    #--------------------------------------------------------------------------
    def test_EUR(self):
        self.assertEqual(u"1,56€", regions.EUR.format_price(Decimal("1.56"), True))
        self.assertEqual(u"0,56€", regions.EUR.format_price(Decimal("0.56"), True))
        self.assertEqual(u"0,99€", regions.EUR.format_price(Decimal("0.99"), True))
        self.assertEqual(u"0,01€", regions.EUR.format_price(Decimal("0.01"), True))

        self.assertEqual(u"1,56€", regions.EUR.format_value(Decimal("1.56"), True))
        self.assertEqual(u"56c", regions.EUR.format_value(Decimal("0.56"), True))
        self.assertEqual(u"99c", regions.EUR.format_value(Decimal("0.99"), True))
        self.assertEqual(u"1c", regions.EUR.format_value(Decimal("0.01"), True))

        self.assertEqual(u"1,56", regions.EUR.format_price(Decimal("1.56")))
        self.assertEqual(u"0,56", regions.EUR.format_price(Decimal("0.56")))
        self.assertEqual(u"0,99", regions.EUR.format_price(Decimal("0.99")))
        self.assertEqual(u"0,01", regions.EUR.format_price(Decimal("0.01")))

        self.assertEqual(u"1,56", regions.EUR.format_value(Decimal("1.56")))
        self.assertEqual(u"56", regions.EUR.format_value(Decimal("0.56")))
        self.assertEqual(u"99", regions.EUR.format_value(Decimal("0.99")))
        self.assertEqual(u"1", regions.EUR.format_value(Decimal("0.01")))

    #--------------------------------------------------------------------------
    def test_RUB(self):
        self.assertEqual(u"2 pуб.", regions.RUB.format_price(Decimal("1.56"), True))
        self.assertEqual(u"1 pуб.", regions.RUB.format_price(Decimal("0.56"), True))
        self.assertEqual(u"1 pуб.", regions.RUB.format_price(Decimal("0.99"), True))
        self.assertEqual(u"0 pуб.", regions.RUB.format_price(Decimal("0.01"), True))

        self.assertEqual(u"1,56 pуб.", regions.RUB.format_value(Decimal("1.56"), True))
        self.assertEqual(u"56 коп.", regions.RUB.format_value(Decimal("0.56"), True))
        self.assertEqual(u"99 коп.", regions.RUB.format_value(Decimal("0.99"), True))
        self.assertEqual(u"1 коп.", regions.RUB.format_value(Decimal("0.01"), True))

        self.assertEqual(u"2", regions.RUB.format_price(Decimal("1.56")))
        self.assertEqual(u"1", regions.RUB.format_price(Decimal("0.56")))
        self.assertEqual(u"1", regions.RUB.format_price(Decimal("0.99")))
        self.assertEqual(u"0", regions.RUB.format_price(Decimal("0.01")))

        self.assertEqual(u"1,56", regions.RUB.format_value(Decimal("1.56")))
        self.assertEqual(u"56", regions.RUB.format_value(Decimal("0.56")))
        self.assertEqual(u"99", regions.RUB.format_value(Decimal("0.99")))
        self.assertEqual(u"1", regions.RUB.format_value(Decimal("0.01")))
