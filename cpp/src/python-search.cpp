#include <gamestradamus/steam/game.hpp>
#include <gamestradamus/steam/search.hpp>

#include <fstream>
#include <iostream>
#include <map>

#include <boost/range/adaptors.hpp>
// include headers that implement a archive in simple binary format
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/python.hpp>
using namespace boost::python;
using namespace gamestradamus::steam;


//------------------------------------------------------------------------------
class LocalSearch
{
public:
    //--------------------------------------------------------------------------
    LocalSearch(){}

    //--------------------------------------------------------------------------
    ~LocalSearch() {
        BOOST_FOREACH(ConsumerTraitBucketSearch *search, my_games | boost::adaptors::map_values) {
            delete search;
        }
    }

    //--------------------------------------------------------------------------
    // Load an index file into the the search
    void add_index(Game::id_t id, std::string filename) {
        ConsumerTraitBucketSearch *ctbs = new ConsumerTraitBucketSearch();
        {
            // create and open an archive for input
            std::ifstream ifs(filename.c_str());
            boost::archive::binary_iarchive ia(ifs);
            // read class state from archive
            ia >> *ctbs;
            // archive and stream closed when destructors are called
        }
        my_games[id] = ctbs;
    }

    //--------------------------------------------------------------------------
    object consumers_similar_to(Game::id_t id, double hours_played, int number_of_results) {
        list retval;
        // TODO: there is probably a more efficient way to do this
        // by not searching for id twice - but this should fix a segfault
        if (my_games.find(id) == my_games.end()) {
            return retval;
        }
        ConsumerTraitBucketSearch *ctbs = my_games[id];
        match_heap_t sorted_matches = ctbs->consumers_similar_to(hours_played, number_of_results);
        while (sorted_matches.size()) {
            dict python_match;
            Match match = sorted_matches.top();
            sorted_matches.pop();
            python_match["steam_id_64"] = match.account_id;
            python_match["hours_played"] = match.hours_played;
            retval.append(python_match);
        }
        return retval;
    }


        
private:
    std::map<int, ConsumerTraitBucketSearch *> my_games;
};

BOOST_PYTHON_MODULE(search)
{
    class_<LocalSearch>("Search")
        .def("add_index", &LocalSearch::add_index)
        .def("consumers_similar_to", &LocalSearch::consumers_similar_to)
    ;
}
